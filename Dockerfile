FROM python:3.10-slim as build

RUN pip install poetry

COPY . ./
RUN poetry export --without-hashes -o requirements.txt
RUN poetry build

FROM python:3.10-slim

WORKDIR /install

COPY --from=build dist/*.whl requirements.txt ./
RUN pip install --no-cache-dir --no-compile -r requirements.txt *.whl

WORKDIR /app

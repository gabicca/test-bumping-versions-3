include .make/dependencies.mk
include .make/oci.mk
include .make/release.mk

PROJECT_NAME = test-bumping-versions-3
PROJECT_PATH = gabicca/test-bumping-versions-3

AUTO_RELEASE = true
TARGET_BRANCH = master
ARTEFACT_TYPE = oci

# W503: line break before binary operator
PYTHON_SWITCHES_FOR_FLAKE8 = --ignore=W503

SKART_DEPS_FILE = skart.toml

SKART_WAIT ?= 120
SKART_REQUERY ?= 5
